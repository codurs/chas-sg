package com.sla.codurs.chas.model;

/**
 * Created by Moistyburger on 21/7/14.
 */
public class CervicalScreeningCentre {
    public int hashCode;
    private String name;
    private double x;
    private double y;
    private String iconURL;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getIconURL() {
        return iconURL;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }
}
